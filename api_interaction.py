from getpass import getpass
import requests
import json


class ApiInteraction(object):
    base_url = ''
    user = ''
    password = ''
    token = ''

    def strip_base_url(self):
        self.base_url = self.base_url.strip()
        if self.base_url[len(self.base_url)-1] == '/':
            self.base_url = self.base_url[:-1]

    def get_user_credentials(self):
        self.base_url = input('Bitte Basis-URL der API angeben: ')
        self.user = input('Nutzername: ')
        self.password = getpass('Passwort: ')
        self.strip_base_url()

    def get_token(self):
        r = requests.post(self.base_url + '/token', auth=(self.user, self.password))
        if r.status_code == 200:
            self.token = r.json()['token']
        elif r.status_code == 401:
            self.get_user_credentials()
            self.get_token()

    def __init__(self):
        self.get_user_credentials()
        self.get_token()

    def send_api_request(self, method: str, endpoint: str, payload: dict = {}):
        if not endpoint[0] == '/':
            endpoint = '/' + endpoint
        headers = {'Authorization': 'Bearer ' + self.token, 'content-type': 'application/json'}
        m = method.lower()
        payload = json.dumps(payload)
        if m == 'get':
            r = requests.get(self.base_url + endpoint, headers=headers, data=payload)
            if r.status_code == 200:
                return r.json()
            elif r.status_code == 401:
                self.get_token()
                r = requests.get(self.base_url + endpoint, headers=headers, data=payload)
                if r.status_code == 200:
                    return r.json()
                else:
                    return False
            else:
                return False
        elif m == 'put':
            r = requests.put(self.base_url + endpoint, headers=headers, data=payload)
            if r.status_code == 200:
                return r.json()
            elif r.status_code == 401:
                self.get_token()
                r = requests.put(self.base_url + endpoint, headers=headers, data=payload)
                if r.status_code == 200:
                    return r.json()
                else:
                    return False
            else:
                return False
        elif m == 'post':
            r = requests.post(self.base_url + endpoint, headers=headers, data=payload)
            if r.status_code == 200:
                return r.json()
            elif r.status_code == 401:
                self.get_token()
                r = requests.post(self.base_url + endpoint, headers=headers, data=payload)
                if r.status_code == 200:
                    return r.json()
                else:
                    return False
            else:
                return False
        elif m == 'delete':
            r = requests.delete(self.base_url + endpoint, headers=headers, data=payload)
            if r.status_code == 200:
                return r.json()
            elif r.status_code == 401:
                self.get_token()
                r = requests.delete(self.base_url + endpoint, headers=headers, data=payload)
                if r.status_code == 200:
                    return r.json()
                else:
                    return False
            else:
                return False
