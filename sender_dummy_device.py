from api_interaction import ApiInteraction
import time
import random

api_interaction = ApiInteraction()


def generate_data():
    x = random.uniform(-0.2, 0.2)
    print(api_interaction.send_api_request('post', '/session/device/DummyDevice', {'value': x}))


interval = 1
while True:
    time.sleep(interval)
    generate_data()
